You'd be surprised how much free time you seem to have when you're reliant on a 3G dongle for your internet fix. When you have to ration out 3GB to last 13 days. So I decided to do a bit of coding.

Specifically, I rewrote an old batch file. It was supposed to check your external IP every 5 minutes and log it to a file. It's a little something I whipped up to prove to BT that my connection was resetting. It doesn't need to do much. One of the things that always bugged me was that it wiped the log every time it started. Probably an easy fix, but why go for an easy fix when you can rewrite it from scratch in a whole new language?!

And now look at the pretty GUI! After the rewrite LogIP serves 2 purposes. Run it and  it'll just grab your external IP and show it to you. Click the button and it will start logging your IP every 5 minutes.
