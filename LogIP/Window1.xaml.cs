﻿// Copyright (c) 2009, Paul Jones
// All rights reserved.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using System.Timers;
using System.Windows.Threading;

namespace LogIP
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            string ip = getIP();
            updateUI(ip, true, "Click the IP Address to Start Logging.");
        }


        private void refresh_Click(object sender, RoutedEventArgs e)
        {
            string ip = getIP();
            using (StreamWriter w = File.AppendText("IP.log"))
            {
                string currentLine = DateTime.Now + ": " + ip;
                w.WriteLine(currentLine);
                w.Close();
            }
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(logIP);
            aTimer.Interval = 300000;
            aTimer.Enabled = true;
            aTimer.AutoReset = true;
            updateUI(ip, false, "Logging IP address to IP.log every 5 minutes.");
        }

        private void logIP(object source, ElapsedEventArgs e)
        {
            string ip = getIP();
            using (StreamWriter w = File.AppendText("IP.log"))
            {
                string currentLine = DateTime.Now + ": " + ip;
                w.WriteLine(currentLine);
                w.Close();
            }
        }

        private void updateUI(string ip, bool enabled, string status)
        {
            refresh.Content = ip;
            refresh.IsEnabled = enabled;
            statusText.Content = status;
        }

        private string getIP()
        {
            // TODO Caching (Only check if not checked in last 5 minutes)
            string ip = retrieveIP();
            return ip;
        }

        private string retrieveIP()
        {
            WebClient client = new WebClient();
            string externalSiteIP = "http://pjon.es/logip.php";
            try
            {
                Stream data = client.OpenRead(externalSiteIP);
                StreamReader reader = new StreamReader(data);
                string ip = reader.ReadToEnd();
                return ip;
            }
            catch (WebException)
            {
                return "No Connection";
            }
        }  
    }
}
